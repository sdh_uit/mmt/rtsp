#include "liveMedia.hh"
#include "BasicUsageEnvironment.hh"

#define HLS_SEGMENTATION_DURATION 6
#define HLS_FILENAME_PREFIX "stream"

FILE* fileStream = NULL;
UsageEnvironment* env;

void segmentationCallback(void* clienData, char const* segmentFilename, double segmentDuration);
void afterPlaying(void* clientData);

int main(int argc, char** argv) {
    TaskScheduler* scheduler = BasicTaskScheduler::createNew();
    env = BasicUsageEnvironment::createNew(*scheduler);
    char fullname[100];
    strcpy(fullname, "../");
    strcat(fullname, argv[1]);

    FramedSource* inputSource = ByteStreamFileSource::createNew(*env, fullname);

    if (inputSource == NULL) {
        *env << "Unable to open file \"" << argv[1] << "\" as a byte-stream file source\n";
        exit(1);
    }

    H264VideoStreamFramer* framer = H264VideoStreamFramer::createNew(*env, inputSource, True, True);

    MPEG2TransportStreamFromESSource* tsFrames = MPEG2TransportStreamFromESSource::createNew(*env);

    tsFrames->addNewVideoSource(framer, 5);

    MediaSink* outputSink = HLSSegmenter::createNew(*env, HLS_SEGMENTATION_DURATION, HLS_FILENAME_PREFIX, segmentationCallback);

    *env << "Beginning to read...\n";

    outputSink->startPlaying(*tsFrames, afterPlaying, NULL);

    env->taskScheduler().doEventLoop();

    return 0;
}

void segmentationCallback(void*, char const* segmentFilename, double segmentDuration) {
    if (fileStream == NULL) {
        char* m3u8Filename = new char[strlen(HLS_FILENAME_PREFIX) + 6];
        sprintf(m3u8Filename, "%s.m3u8", HLS_FILENAME_PREFIX);
        fileStream = fopen(m3u8Filename, "wb");

        fprintf(fileStream,
            "#EXTM3U\n"
            "#EXT-X-VERSION:3\n"
            "#EXT-X-INDEPENDENT-SEGMENTS\n"
            "#EXT-X-TARGETDURATION:%u\n"
            "#EXT-X-MEDIA-SEQUENCE:0\n",
            HLS_SEGMENTATION_DURATION);
    }

    fprintf(fileStream,
        "#EXTINF:%f,\n"
        "%s\n",
        segmentDuration,
        segmentFilename);

    fprintf(stderr, "Wrote segment \"%s\" (duration: %f seconds)\n", segmentFilename, segmentDuration);
}

void afterPlaying(void*) {
    *env << "...Done reading\n";
    fprintf(fileStream, "#EXT-X-ENDLIST\n");
    fprintf(stderr, "Wrote %s.m3u8\n", HLS_FILENAME_PREFIX);
    exit(0);
}
