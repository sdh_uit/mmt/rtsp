const bodyParser = require('body-parser');
const crypto = require('crypto');
const express = require('express');
const formidable = require('formidable');
const fs = require('fs');
const path = require('path');
const sqlite3 = require('sqlite3').verbose();

const { exec,spawn } = require('child_process');

const rootPath = path.dirname(__filename);
const dbPath = path.join(rootPath, 'data', 'storage.db');
const filestorePath = path.join(rootPath, '..', 'resources');
const port = 8080;
const app = express();

app.set('view engine', 'ejs');
app.set('views', 'views');
app.disable('etag');

app.use(express.static(path.join(rootPath, 'node_modules')));
app.use(express.static(path.join(rootPath, 'public')));
app.use(express.static(filestorePath));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));


app.get('/', (req, res) => {
    res.render('html', {
        title: 'RTSP Server',
        content: {
            template: 'main',
            data: { hasStream: false }
        }

    });
});

app.get('/files', (req, res) => {
    const db = new sqlite3.Database(dbPath);
    db.all('SELECT id, title, name, hash, path FROM FILE', (err, rows) => {
        if (err) {
            rows = [];
        }

        res.render('html', {
            title: 'Files',
            content: {
                template: 'files',
                data: {
                    error: req.query.error,
                    success: req.query.success,
                    files: rows
                }
            }
        });
    });
});

app.get('/api/files', (req, res) => {
    const db = new sqlite3.Database(dbPath);
    db.all('SELECT id, title, name, hash, path FROM FILE', (err, rows) => {
        if (err) {
            rows = [];
        }

        res.json(rows);
    });
});

app.get('/stream/:fileId', (req, res) => {
    const fileId = req.params.fileId;
    const db = new sqlite3.Database(dbPath);
    db.get('SELECT title, hash FROM FILE WHERE id = ?', [fileId], (err, row) => {
        if (err || !row) {
            return res.redirect(302, '/');
        }

        res.render('html', {
            title: row.title,
            content: {
                template: 'main',
                data: {
                    hasStream: true,
                    src: `/${row.hash}/stream.m3u8`
                }
            }
        });
    });
});

app.post('/files/upload', (req, res) => {
    const form = formidable({ multiples: true });
    form.parse(req, (err, fields, files) => {
        if (err) {
            return res.redirect(302, '/files?error=1');
        }

        const file = fs.readFileSync(files.uploadFile.path);
        const checksum = crypto.createHash('md5').update(file).digest('hex');
        const ext = files.uploadFile.name.split('.').pop();
        const filename = `${checksum}.${ext}`;
        const filePath = path.join(filestorePath, filename);

        fs.access(filePath, fs.constants.F_OK, err => {
            if (err) {
                let cmd;
                fs.copyFileSync(files.uploadFile.path, filePath);
                switch (ext) {
                    case '264':
                        const segmentsFolder = path.join(filestorePath, checksum);
                        fs.mkdirSync(segmentsFolder);
                        cmd = path.join(rootPath, '..', 'live', 'testProgs', 'h264tohls');
                        spawn(cmd, [filename], { cwd: segmentsFolder });
                        break;
                    case 'ts':
                        cmd = path.join(rootPath, '..', 'live', 'testProgs', 'MPEG2TransportStreamIndexer');
                        spawn(cmd, [filename], { cwd: filestorePath });
                        break;
                    default:
                        break;
                }
            }

            const db = new sqlite3.Database(dbPath);
            db.serialize(() => {
                db.run(
                    'INSERT INTO FILE(title, name, hash, path) VALUES(?, ?, ?, ?)',
                    fields.title,
                    files.uploadFile.name,
                    checksum,
                    `rtsp://${req.hostname}/${filename}`,
                    err => {
                        db.close();
                        if (err) {
                            return res.redirect(302, '/files?error=1');
                        }
                        res.redirect(302, '/files?success=1');
                    }
                );
            });
        });
    });
});

app.post('/files/delete', (req, res) => {
    const fileId = req.body.fileId;
    const db = new sqlite3.Database(dbPath);
    db.serialize(() => {
        db.get('SELECT id, hash FROM FILE WHERE id = ?', [fileId], (err, row) => {
            if (err || !row) {
                return res.render('html', {
                    title: 'File not found',
                    content: {
                        template: '404',
                        data: {}
                    }
                });
            }
            const fullpath = path.join(filestorePath, row.hash);
            exec(`rm -rf ${fullpath}*`, err => console.log(err));
            db.run('DELETE FROM FILE WHERE id = ?', [fileId], err => {
                db.close()
                res.redirect(302, '/files');
            });
        });
    });
});

app.listen(port, () => console.log(`Server running.\nOpen URL http://localhost:${port} to review`));
