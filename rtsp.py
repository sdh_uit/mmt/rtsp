
import re
import socket
import sys

HOST = 'leonard-it.com'
PORT = 554
FILE = 'bc21f0ce16d988343303b9e6216e618d.264'
SIZE = 4096


def connect():
    s = None
    for res in socket.getaddrinfo(
            HOST, PORT, socket.AF_UNSPEC, socket.SOCK_STREAM):
        af, socktype, proto, canonname, sa = res

        try:
            s = socket.socket(af, socktype, proto)
        except OSError:
            continue

        try:
            s.connect(sa)
        except OSError:
            s.close()
            s = None
            continue

        break

    if not s:
        print(f'Cannot connect to server {HOST}')
        exit(1)

    return s


def req_log(msg):
    print('Request message:')
    print(msg)


def res_log(msg):
    print('Response message:')
    print(msg)


def send(msg, cb=None):
    req_log(msg)

    with connect() as s:
        s.sendall(msg.encode())
        res = s.recv(SIZE).decode()
        res_log(res)

        if cb and callable(cb):
            cb(res)


def send_options(argv):
    msg = (
        f'OPTIONS rtsp://{HOST}:{PORT}/{FILE} RTSP/1.0\r\n'
        'CSeq: 1\r\n'
        '\r\n'
    ).format(HOST, PORT, FILE)

    send(msg)
    exit(0)


def send_describe(argv):
    msg = (
        f'DESCRIBE rtsp://{HOST}:{PORT}/{FILE} RTSP/1.0\r\n'
        'CSeq: 2\r\n'
        '\r\n'
    )

    send(msg)
    exit(0)


def send_setup(argv):
    def export_session(res):
        session_obj = re.search(r'^Session: ([0-9a-zA-Z]*);.*$', res, re.M)
        if session_obj:
            print(f'Active session: {session_obj.group(1)}')

    msg = (
        f'SETUP rtsp://{HOST}:{PORT}/{FILE} RTSP/1.0\r\n'
        'CSeq: 3\r\n'
        '\r\n'
    )

    send(msg, export_session)
    exit(0)


def send_play(argv):
    if not argv:
        usage()
        exit(1)

    [session] = argv[:1]
    argv = argv[1:]

    msg = (
        f'PLAY rtsp://{HOST}:{PORT}/{FILE} RTSP/1.0\r\n'
        'CSeq: 4\r\n'
        f'Session: {session}\r\n'
    )

    if argv:
        [time] = argv[:1]
        msg += f'Range: ntp={time}-\r\n'

    msg += '\r\n'
    send(msg)
    exit(0)


def send_pause(argv):
    if not argv:
        usage()
        exit(1)

    [session] = argv[:1]
    argv = argv[1:]

    msg = (
        f'PAUSE rtsp://{HOST}:{PORT}/{FILE} RTSP/1.0\r\n'
        'CSeq: 5\r\n'
        f'Session: {session}\r\n'
        '\r\n'
    )

    send(msg)
    exit(0)


def send_teardown(argv):
    if not argv:
        usage()
        exit(1)

    [session] = argv[:1]
    argv = argv[1:]

    msg = (
        f'TEARDOWN rtsp://{HOST}:{PORT}/{FILE} RTSP/1.0\r\n'
        'CSeq: 6\r\n'
        f'Session: {session}\r\n'
        '\r\n'
    )

    send(msg)
    exit(0)


def usage():
    msg = (
        'RTSP Commands:\r\n'
        '\tOPTIONS\r\n'
        '\tDESCRIBE\r\n'
        '\tSETUP\r\n'
        '\tPLAY SESSION [TIME]\r\n'
        '\tPAUSE SESSION\r\n'
        '\tTEARDOWN SESSION\r\n'
    )
    print(msg)


def main(argv):
    if not argv:
        argv = ['usage']

    [cmd] = argv[:1]
    argv = argv[1:]
    cmd = cmd.upper()

    if cmd == 'OPTIONS':
        send_options(argv)
    elif cmd == 'DESCRIBE':
        send_describe(argv)
    elif cmd == 'SETUP':
        send_setup(argv)
    elif cmd == 'PLAY':
        send_play(argv)
    elif cmd == 'PAUSE':
        send_pause(argv)
    elif cmd == 'TEARDOWN':
        send_teardown(argv)

    usage()
    exit(1)


if __name__ == '__main__':
    main(sys.argv[1:])
